# SLAMDrones #

This is the readme for our senior capstone project.

### What is this repository for? ###

* This repositories marks the published code for our senior project. 
* This is the code we used to build and demonstrate our project's capabilities. 
* Version 1.1.0
* [Our Website](https://slamdrones.blogspot.com)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

* Ben Antczak (bnatelly'at'gmail.com
* Dusty Argyle (dustymargyle'at'gmail.com)
* Nick Hallstrom (cocolymoo'at'gmail.com)